package com.example.demo;


import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

public class DojoStreamTest {

    @Test
    void converterData(){
        List<Player> list = CsvUtilFile.getPlayers();
        assert list.size() == 18207;
    }

    @Test
    void jugadoresMayoresA35(){
        List<Player> list = CsvUtilFile.getPlayers();
        Set<Player> result = list.stream()
                .filter(jugador -> jugador.getAge() > 35)
                .collect(Collectors.toSet());
        result.forEach(System.out::println);
    }

    @Test
    void jugadoresMayoresA35SegunClub(){
        List<Player> list = CsvUtilFile.getPlayers();
        Map<String, List<Player>> result = list.stream()
                .filter(player -> player.getAge() > 35)
                .distinct()
                .collect(Collectors.groupingBy(Player::getClub));

        result.forEach((key, jugadores) -> {
            System.out.println("\n");
            System.out.println(key + ": ");
            jugadores.forEach(System.out::println);
        });

    }

    @Test
    void mejorJugadorConNacionalidadFrancia(){
        List<Player> list = CsvUtilFile.getPlayers();
        Player result = list.stream()
                .filter(player -> player.getNational().equals("France"))
                .max((player1, player2) -> Integer.compare(player1.getWinners(), player2.getWinners()))
                .orElse(null);
                if(result != null) System.out.println("El mejor jugador es" + result.getName());
                System.out.println(result);
    }


    @Test
    void clubsAgrupadosPorNacionalidad(){
        List<Player> list = CsvUtilFile.getPlayers();
        Map<String, List<Player>> result = list.stream()
                .distinct()
                .collect(Collectors.groupingBy(Player::getNational));
        result.forEach((key, clubs) -> {
            System.out.println("\n");
            System.out.println(key + ": ");
            clubs.forEach((club) -> {
                System.out.println("club " + club.getClub() + " nacionalidad " + club.getNational());
            });
        });
    }

    @Test
    void clubConElMejorJugador(){
        List<Player> list = CsvUtilFile.getPlayers();
        Player result = list.stream()
                .max((player1, player2) -> Integer.compare(player1.getWinners(), player2.getWinners()))
                .orElse(null);
        if(result != null) System.out.println("El club con el mejor jugador es " + result.getClub());
        System.out.println(result);
    }

    @Test
    void ElMejorJugador(){
        List<Player> list = CsvUtilFile.getPlayers();
        Player result = list.stream()
                .max((player1, player2) -> Integer.compare(player1.getWinners(), player2.getWinners()))
                .orElse(null);
        if(result != null) System.out.println("El mejor jugador es" + result.getName());
        System.out.println(result);
    }

    @Test
    void mejorJugadorSegunNacionalidad(){
        List<Player> list = CsvUtilFile.getPlayers();
        Map<String, Optional<Player>> result = list.stream()
                .collect(Collectors.groupingBy(Player::getNational,
                        Collectors.maxBy(Comparator.comparingInt(Player::getWinners))));

        result.forEach((nacionalidad, jugador) -> {
            Player winnerPlayer = jugador.get();
            System.out.println("\n");
            System.out.println("jugador " + winnerPlayer.getName() +
                    " de nacionalidad " + nacionalidad +
                    " con " + winnerPlayer.getWinners() +" victorias");
        });
    }


}
